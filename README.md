# Examining the Hydra

This is a repository contains and links to software artificates for the
following research publication: `Examining the Hydra: Simultaneously Shared
Links in Tor and the Effects on its Performance`.

If you want to references this work you can use this biblatex entry:

```
@inproceedings{hydra-popets2023,
  title     = {{Examining the Hydra: Simultaneously Shared Links in Tor and the Effects on its Performance}},
  shorttile = {{Examining the Hydra}},
  author    = {{Pahl, Sebastian and Adamsky, Florian and Kaiser, Daniel and Engel, Thomas}}
  booktitle = {{Proceedings of the 23\textsuperscript{th} on Privacy Enhancing Technologies Symposium (PETS)}},
  year      = {2023},
  url       = {{https://gitlab.com/spahl/hydra-popets2023.git}},
}
```

## Overview

* This repository contains notebooks, SALSA and simulation releated code. The
  notebooks in `analysis/*.ipynb` are intended for reference, require data from
  experiments, and may require small changes. They show how the figures were
  created in the paper.

* [tor](https://gitlab.com/spahl/tor): contains modifications to Tor from other
  research projects as well as our own small modifictions.
  These are used by the following projects. Notable branches are:
   * [pctcp](https://gitlab.com/spahl/tor/-/tree/pctcp)
   * [quictor](https://gitlab.com/spahl/tor/-/tree/quictor)
   * [modtor](https://gitlab.com/spahl/tor/-/tree/modtor)
   * [pctls](https://gitlab.com/spahl/tor/-/tree/pctls)
   * [vanilla](https://gitlab.com/spahl/tor/-/tree/vanilla)
   * [vanilla-logging](https://gitlab.com/spahl/tor/-/tree/vanilla-logging)

* [tor-mininet](https://gitlab.com/spahl/tor-mininet): contains a local testbed
  system based on mininet that is similar to
  [Chutney](https://github.com/torproject/chutney); in fact, we use Chutney,
  but only for generating configuration for the nodes; but allows more fine
  control over the topology, bandwidth and packet loss. We use this for our
  local emulations.

* [tor-neverenough-docker](https://gitlab.com/spahl/tor-neverenough-docker):
  packs all dependencies of the paper "Once is Never Enough: Foundations for
  Sound Statistical Inference in Tor Network Experimentation" into a docker
  container. We use an older version of Shadow 1.3. We use it for our
  Shadow simulations.

  *Note*: For newer experiments you should use Shadow 2.0. With this version,
  compiling an extra Tor plugin is no longer necessary, binaries can be used
  directly and Docker is supported.

## Setup

```sh
$ vagrant up
$ vagrant ssh
$ cd /vagrant
```

## Examples

```sh
make init
```

### TorPS

```sh
$ make init-torps-2021-11
$ make run-torps-2021-11
```

Results are in `data/log.2021-11.torps`.
Readable with the `LogTorPS` class in `analysis/lib.py`.

### ModTor

```sh
$ make run-for-november
```

Readable with the `Log` class in `analysis/lib.py`.

### SALSA (test)

```sh
$ cd archive
$ tar xvf archive/microdescs-2021-11.tar.xz
$ cd ..
$ python3 analysis/test.py
```
