.PHONY: init

init:
	mkdir -p data
	mkdir -p archive/in
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-2021-11.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2021-11.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2021-10.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-2021-01.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2021-01.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2020-12.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-2013-06.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2013-06.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/server-descriptors/server-descriptors-2013-05.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/consensuses/consensuses-2016-04.tar.xz
	find archive/ -name 'consensuses-*.tar.xz'        | xargs -t -n 1 tar --skip-old-files -C archive/in -xf
	find archive/ -name 'server-descriptors-*.tar.xz' | xargs -t -n 1 tar --skip-old-files -C archive/in -xf
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/certs.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2022-01.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-12.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-11.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-10.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-09.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-08.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-07.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-06.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-05.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-04.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-03.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-02.tar.xz
	wget -c -P archive/ https://collector.torproject.org/archive/relay-descriptors/microdescs/microdescs-2021-01.tar.xz

init-torps:
	python2 vendor/torps/pathsim.py process --start_year 2021 --start_month 1 --end_year 2021 --end_month 1 --in_dir archive/in --out_dir data/ --initial_descriptor_dir archive/in/server-descriptors-2020-12

init-torps-2013:
	python2 vendor/torps/pathsim.py process --start_year 2013 --start_month 6 --end_year 2013 --end_month 6 --in_dir archive/in --out_dir data/ --initial_descriptor_dir archive/in/server-descriptors-2013-06

init-torps-2021-11:
	python2 vendor/torps/pathsim.py process --start_year 2021 --start_month 11 --end_year 2021 --end_month 11 --in_dir archive/in --out_dir data/ --initial_descriptor_dir archive/in/server-descriptors-2021-10

run-torps:
	python2 vendor/torps/pathsim.py simulate --nsf_dir data/network-state-2021-01 --num_samples 950100 --user_model simple=31536000 --format normal tor > data/log.torps

run-torps-2013:
	python2 vendor/torps/pathsim.py simulate --nsf_dir data/network-state-2013-06 --num_samples 950100 --user_model simple=31536000 --format normal tor > data/log.2013.torps

run-torps-2021-11:
	python2 vendor/torps/pathsim.py simulate --nsf_dir data/network-state-2021-11 --num_samples 200000 --user_model simple=86400 --format normal tor > data/log.2021-11.torps

init-torps-2021-11-perf:
	mkdir -p data/perf
	echo "start `date`" >> data/perf/init.log
	./bin/torps torps/pathsim.py process --start_year 2021 --start_month 11 --end_year 2021 --end_month 11 --in_dir archive/in --out_dir data/perf --initial_descriptor_dir archive/in/server-descriptors-2021-10
	echo "end `date`" >> data/perf/init.log
	sudo chown seb:seb data/perf -R

run-torps-2021-11-perf:
	echo "start `date`" >> data/perf/run.log
	./bin/torps torps/pathsim.py simulate --nsf_dir data/perf/network-state-2021-11 --num_samples 1000000 --user_model simple=86400 --format normal tor > data/perf/log.2021-11.torps
	echo "end `date`" >> data/perf/run.log

run-per-month-perf:
	sudo parallel -j `nproc` -k python scripts/modtor.py ::: \
		'2021-01-01 00:06:00' \
		'2021-02-01 00:06:00' \
		'2021-03-01 00:06:00' \
		'2021-04-01 00:06:00' \
		'2021-05-01 00:06:00' \
		'2021-06-01 00:06:00' \
		'2021-07-01 00:06:00' \
		'2021-08-01 00:06:00' \
		'2021-09-01 00:06:00' \
		'2021-10-01 00:06:00' \
		'2021-11-01 00:06:00' \
		'2021-12-01 00:06:00'

run-per-month:
	sudo parallel -j `nproc` -k python scripts/modtor.py ::: \
		'2021-01-01 00:00:00' \
		'2021-02-01 00:00:00' \
		'2021-03-01 00:00:00' \
		'2021-04-01 00:00:00' \
		'2021-05-01 00:00:00' \
		'2021-06-01 00:00:00' \
		'2021-07-01 00:00:00' \
		'2021-08-01 00:00:00' \
		'2021-09-01 00:00:00' \
		'2021-10-01 00:00:00' \
		'2021-11-01 00:00:00' \
		'2021-12-01 00:00:00'

run-internal-per-month:
	sudo parallel -j `nproc` -k python scripts/modtor.py --internal ::: \
		'2021-01-01 00:01:00' \
		'2021-02-01 00:01:00' \
		'2021-03-01 00:01:00' \
		'2021-04-01 00:01:00' \
		'2021-05-01 00:01:00' \
		'2021-06-01 00:01:00' \
		'2021-07-01 00:01:00' \
		'2021-08-01 00:01:00' \
		'2021-09-01 00:01:00' \
		'2021-10-01 00:01:00' \
		'2021-11-01 00:01:00' \
		'2021-12-01 00:01:00'

run-for-day:
	sudo parallel -j `nproc` -k python3 scripts/modtor.py ::: \
		'2021-01-02 00:00:01' \
		'2021-01-02 00:00:02' \
		'2021-01-02 00:00:03' \
		'2021-01-02 00:00:04' \
		'2021-01-02 00:00:05' \
		'2021-01-02 00:00:06' \
		'2021-01-02 00:00:07' \
		'2021-01-02 00:00:08' \
		'2021-01-02 00:00:09' \
		'2021-01-02 00:00:10' \
		'2021-01-02 00:00:11' \
		'2021-01-02 00:00:12'

run-for-november:
	sudo parallel -j `nproc` -k python3 scripts/modtor.py ::: \
		'2021-11-01 00:02:00' \
		'2021-11-02 00:02:00' \
		'2021-11-03 00:02:00' \
		'2021-11-04 00:02:00' \
		'2021-11-05 00:02:00' \
		'2021-11-06 00:02:00' \
		'2021-11-07 00:02:00' \
		'2021-11-08 00:02:00' \
		'2021-11-09 00:02:00' \
		'2021-11-10 00:02:00' \
		'2021-11-11 00:02:00' \
		'2021-11-12 00:02:00' \
		'2021-11-13 00:02:00' \
		'2021-11-14 00:02:00' \
		'2021-11-15 00:02:00' \
		'2021-11-16 00:02:00' \
		'2021-11-17 00:02:00' \
		'2021-11-18 00:02:00' \
		'2021-11-19 00:02:00' \
		'2021-11-20 00:02:00' \
		'2021-11-21 00:02:00' \
		'2021-11-22 00:02:00' \
		'2021-11-23 00:02:00' \
		'2021-11-24 00:02:00' \
		'2021-11-25 00:02:00' \
		'2021-11-26 00:02:00' \
		'2021-11-27 00:02:00' \
		'2021-11-28 00:02:00' \
		'2021-11-29 00:02:00' \
		'2021-11-30 00:02:00'
