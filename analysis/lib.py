import os
import base64
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from tornettools.generate_tor import get_relays
from tornettools.generate_tor import __recompute_bwweights as recompute_bwweights
from tornettools.generate_tor import __get_min as get_min

from numpy.random import choice
from datetime import datetime
from collections import defaultdict
from collections import Counter
from stem import Flag
from stem.exit_policy import MicroExitPolicy
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.router_status_entry import RouterStatusEntryV3

ACTIVE_CIRCUITS_PER_SECOND=24219*4
ACTIVE_CIRCUITS_PER_SECOND_STD=155*4


def convert_fingerprint_hex_to_base64(fingerprint):
    fingerprint = int(fingerprint, 16)
    fingerprint = fingerprint.to_bytes(20, byteorder='big')
    fingerprint = base64.b64encode(fingerprint).decode()
    fingerprint = fingerprint.replace('=', '')
    return fingerprint


def convert_fingerprint_base64_to_hex(fingerprint):
    fingerprint += '='
    fingerprint = bytes(fingerprint, 'utf8')
    fingerprint = base64.b64decode(fingerprint)
    return fingerprint.hex().upper()


assert convert_fingerprint_base64_to_hex(convert_fingerprint_hex_to_base64('1B7952B0066F917301B2973D0D0D4D868D63FCBB')) == '1B7952B0066F917301B2973D0D0D4D868D63FCBB'
assert convert_fingerprint_base64_to_hex(convert_fingerprint_hex_to_base64('97755455E7DC4E95F0F2EE660FCF11726B6BF69A')) == '97755455E7DC4E95F0F2EE660FCF11726B6BF69A'


def plot_link_distribution(ax, date, values, label='', max_bin=None, ylim=(1, 10**7)):
    if not max_bin:
        max_bin = max(values)
    ax.grid()
    ax.set_yscale('log')
    ax.set_ylim(*ylim)
    ax.hist(values, bins=range(1, max_bin), histtype='step', label=label)


def shared_unshared_circuit_paths(links):
    total = shared = unshared = 0
    for count in links.values():
        total += count
        if count > 1:
            shared += count
        else:
            unshared += 1
    assert (shared+unshared) == total
    return total, shared, unshared


def shared_unshared_links(links):
    total = shared = unshared = 0
    for count in links.values():
        total += 1
        if count > 1:
            shared += 1
        else:
            unshared += 1
    assert (shared+unshared) == total
    return total, shared, unshared


class Stat:
    def __init__(self, label, date, directed_links):
        self.label = label
        self.date = date

        self.links = defaultdict(int)

        self.guard_middles = defaultdict(int)
        self.middle_exits = defaultdict(int)

        self.circuits = defaultdict(int)

        self.guards = defaultdict(int)
        self.middles = defaultdict(int)
        self.exits = defaultdict(int)

        self.relays = defaultdict(int)

        self.total = 0

        self.consensus = None
        self.directed_links = directed_links

    def create(circuits, label='', date=None, consensus=None, directed_links=True):
        stat = Stat(label, date, directed_links)
        stat.consensus = consensus
        stat.directed_links = directed_links

        for guard, middle, exit in circuits:
            stat.total += 1

            stat.guards[guard] += 1
            stat.middles[middle] += 1
            stat.exits[exit] += 1

            stat.relays[guard] += 1
            stat.relays[middle] += 1
            stat.relays[exit] += 1

            if directed_links:
                GM = tuple([guard, middle])
                ME = tuple([middle, exit])
            else:
                GM = frozenset([guard, middle])
                ME = frozenset([middle, exit])

            GME = tuple([guard, middle, exit])

            stat.guard_middles[GM] += 1
            stat.middle_exits[ME] += 1

            stat.circuits[GME] += 1

            stat.links[GM] += 1
            stat.links[ME] += 1

        return stat

    def sample_relays_by_count(relays, N):
        values = np.array(list(relays.values()))
        p = values / np.sum(values)
        samples = choice(list(relays.keys()), N, p=p)
        return samples

    def plot(self, ax, kind='GME', max_bin=None, ylim=(1, 10**7)):
        if kind == 'GME':
            values = self.links.values()
        elif kind == 'GM':
            values = self.guard_middles.values()
        elif kind == 'ME':
            values = self.middle_exits.values()

        plot_link_distribution(ax, self.date, list(values), label=self.label, max_bin=max_bin, ylim=ylim)

    def create_table(self):
        table = dict()
        table['Date'] = self.date
        # table['Label'] = self.label
        table['Total Circuits'] = self.total

        total, shared, unshared = shared_unshared_links(self.links)
        table['Total Links'] = total
        table['Shared Links'] = shared
        table['Unshared Links'] = unshared
        table['Shared Links Percent'] = shared / total * 100

        total, shared, unshared = shared_unshared_circuit_paths(self.links)
        assert total == self.total * 2
        table['Total Circuit Parts'] = total
        table['Shared Circuit Parts'] = shared
        table['Unshared Circuit Parts'] = unshared
        table['Shared Circuit Parts Percent'] = shared / total * 100

        total, shared, unshared = shared_unshared_links(self.guard_middles)
        table['Total Guard-Middle Links'] = total
        table['Shared Guard-Middles Links'] = shared
        table['Unshared Guard-Middles Links'] = unshared
        table['Shared Guard-Middles Percent'] = shared / total * 100

        total, shared, unshared = shared_unshared_links(self.middle_exits)
        table['Total Middle-Exits Links'] = total
        table['Shared Middle-Exits Links'] = shared
        table['Unshared Middle-Exits Links'] = unshared
        table['Shared Middle-Exits Links Percent'] = shared / total * 100

        total, shared, unshared = shared_unshared_links(self.circuits)
        table['Complete Shared Circuits'] = shared

        table['Relays'] = len(self.relays)
        table['Guards'] = len(self.guards)
        table['Middles'] = len(self.middles)
        table['Exits'] = len(self.exits)

        if isinstance(self.consensus, Consensus):
            table['Consensus Relays'] = len(self.consensus.relays())
            table['Consensus Guards'] = len(self.consensus.guards())
            table['Consensus Middles'] = len(self.consensus.middles())
            table['Consensus Exits'] = len(self.consensus.exits())

        df = pd.DataFrame([table.values()], columns=table.keys())
        df = df.set_index('Date')
        return df


class Log:
    def __init__(self, filename):
        self.filename = filename

        path = os.path.normpath(filename)
        pieces = path.split(os.sep)
        assert len(pieces) >= 2
        self.date = datetime.strptime(pieces[-2], '%Y-%m-%d-%H-%M-%S')

        self.tor_version = None
        self.stat = None

    def parse(self, take_n=ACTIVE_CIRCUITS_PER_SECOND):
        with open(self.filename) as log:
            i = 0
            for line in log.readlines():
                if '[notice] Tor 0.' in line:
                    index = line.index('Tor ')
                    index_next_space = line.index(' ', index+4)
                    self.tor_version = line[index+4:index_next_space]
                if not 'CIRC:exit' in line and not 'CIRC:internal' in line:
                    continue

                pieces = line.split(":")
                pieces[-1] = pieces[-1].replace('(closed)', '')
                pieces[-1] = pieces[-1].replace('\n', '')
                path = pieces[-1].split(' ')
                path = path[1:]

                if take_n is not None and i > take_n:
                    break
                i += 1

                yield path

    def read(filename, consensus=None, take_n=ACTIVE_CIRCUITS_PER_SECOND, directed_links=True):
        log = Log(filename)
        stat = Stat.create(log.parse(take_n), consensus=consensus, directed_links=directed_links)
        stat.date = log.date
        log.stat = stat
        return log


class LogTorPS:
    def __init__(self, filename):
        self.filename = filename
        self.date = None
        self.tor_version = 'TorPS'
        self.stat = None

    def parse(self):
        df = pd.read_csv(self.filename, delimiter='\t')
        self.date = datetime.utcfromtimestamp(df.iloc[0]['Timestamp'])
        for index, row in df.iterrows():
            yield row['Guard IP'], row['Middle IP'], row['Exit IP']

    def read(filename, consensus=None, directed_links=True):
        log = LogTorPS(filename)
        stat = Stat.create(log.parse(), consensus=consensus, directed_links=directed_links)
        stat.date = log.date
        log.stat = stat
        return log


class LogTorPSMultiple:
    def parse(df):
        for index, row in df.iterrows():
            yield row['Guard IP'], row['Middle IP'], row['Exit IP']

    def read(filename, split_every_n=100_000, take_n=ACTIVE_CIRCUITS_PER_SECOND, consensus=None, directed_links=True):
        assert take_n <= split_every_n

        df = pd.read_csv(filename, delimiter='\t')
        logs = []
        last_idx = 0
        for idx in range(split_every_n, len(df), split_every_n):
            df_part = df.iloc[last_idx:idx, :]
            df_part = df_part.iloc[:take_n, :]
            last_idx = idx

            log = LogTorPS(filename)
            log.date = datetime.utcfromtimestamp(df_part.iloc[0]['Timestamp'])
            stat = Stat.create(LogTorPSMultiple.parse(df_part), consensus=consensus, directed_links=directed_links)
            stat.date = log.date
            log.stat = stat
            logs.append(log)

        return logs

# From <https://github.com/torproject/torspec/blob/main/dir-spec.txt>:
#
# Wgg - Weight for Guard-flagged nodes in the guard position
# Wgm - Weight for non-flagged nodes in the guard Position
# Wgd - Weight for Guard+Exit-flagged nodes in the guard Position

# Wmg - Weight for Guard-flagged nodes in the middle Position
# Wmm - Weight for non-flagged nodes in the middle Position
# Wme - Weight for Exit-flagged nodes in the middle Position
# Wmd - Weight for Guard+Exit flagged nodes in the middle Position

# Weg - Weight for Guard flagged nodes in the exit Position
# Wem - Weight for non-flagged nodes in the exit Position
# Wee - Weight for Exit-flagged nodes in the exit Position
# Wed - Weight for Guard+Exit-flagged nodes in the exit Position

# From <https://github.com/torps/torps/blob/master/pathsim.py#L191>:
def get_bw_weight(flags, position, bw_weights):
    """Returns weight to apply to relay's bandwidth for given position.
        flags: list of Flag values for relay from a consensus
        position: position for which to find selection weight,
             one of 'g' for guard, 'm' for middle, and 'e' for exit
        bw_weights: bandwidth_weights from NetworkStatusDocumentV3 consensus
    """

    if (position == 'G'):
        if (Flag.GUARD in flags) and (Flag.EXIT in flags):
            weight = bw_weights['Wgd']
        elif (Flag.GUARD in flags):
            weight = bw_weights['Wgg']
        elif (Flag.EXIT not in flags):
            weight = bw_weights['Wgm']
        else:
            weight = 0
    elif (position == 'M'):
        if (Flag.GUARD in flags) and (Flag.EXIT in flags):
            weight = bw_weights['Wmd']
        elif (Flag.GUARD in flags):
            weight = bw_weights['Wmg']
        elif (Flag.EXIT in flags):
            weight = bw_weights['Wme']
        else:
            weight = bw_weights['Wmm']
    elif (position == 'E'):
        if (Flag.GUARD in flags) and (Flag.EXIT in flags):
            weight = bw_weights['Wed']
        elif (Flag.GUARD in flags):
            weight = bw_weights['Weg']
        elif (Flag.EXIT in flags):
            weight = bw_weights['Wee']
        else:
            weight = bw_weights['Wem']
    else:
        raise ValueError('get_weight does not support position {0}.'.format(
            position))

    return float(weight)


class Args:
    def __init__(self, relay_info_path, network_scale):
        self.relay_info_path = relay_info_path
        self.network_scale = network_scale


class BWOnly:
    def __init__(self, bandwidth_weights):
        self.bandwidth_weights=bandwidth_weights


class Consensus:
    def __init__(self, df, consensus, date):
        self.df = df
        self.consensus = consensus
        self.date = date

    # NOTE: All ports must be allowed!
    def match_all_ports_exit_policy(relay, filter_by_exit_port_ranges):
        match = True
        for from_port, to_port in filter_by_exit_port_ranges:
            for port in range(from_port, to_port+1):
                match &= relay.exit_policy.can_exit_to(address=None, port=port)
                if not match:
                    return match
        return match

    def read(filename, filter_by_exit_port_ranges=None):
        consensus_table = defaultdict(list)
        flags = set()

        consensus = next(parse_file(
            filename,
            descriptor_type = 'network-status-consensus-3 1.0',
            document_handler = DocumentHandler.DOCUMENT,
        ))

        for fingerprint, relay in consensus.routers.items():
            if 'Exit' in relay.flags and filter_by_exit_port_ranges and not Consensus.match_all_ports_exit_policy(relay, filter_by_exit_port_ranges):
                continue
            fingerprint_base64 = base64.b64encode(bytes.fromhex(fingerprint)).decode('utf8').replace('=', '')
            consensus_table['ID'].append(fingerprint_base64)
            consensus_table['Nickname'].append(relay.nickname)
            consensus_table['Version'].append(relay.version)

            is_guard = 'Guard' in relay.flags
            is_middle = not 'Guard' in relay.flags and not 'Exit' in relay.flags
            is_exit = 'Exit' in relay.flags

            consensus_table['Address'].append(relay.address)
            consensus_table['Subnet'].append('.'.join(relay.address.split('.')[0:2]))
            consensus_table['DigestHex'].append(relay.digest)
            consensus_table['Fast'].append('Fast' in relay.flags)
            consensus_table['Authority'].append('Authority' in relay.flags)
            consensus_table['Guard'].append(is_guard)
            consensus_table['Middle'].append(is_middle)
            consensus_table['Exit'].append(is_exit)
            consensus_table['V2Dir'].append('V2Dir' in relay.flags)
            consensus_table['Stable'].append('Stable' in relay.flags)
            consensus_table['HSDir'].append('HSDir' in relay.flags)
            consensus_table['BadExit'].append('BadExit' in relay.flags)
            consensus_table['StaleDesc'].append('StaleDesc' in relay.flags)
            consensus_table['Running'].append('Running' in relay.flags)
            consensus_table['Valid'].append('Valid' in relay.flags)
            consensus_table['Bandwidth'].append(relay.bandwidth)
            consensus_table['Unmeasured'].append(relay.is_unmeasured)

            if is_middle:
                consensus_table['WeightForGuard'].append(0.0)
            else:
                consensus_table['WeightForGuard'].append(relay.bandwidth * (get_bw_weight(relay.flags, 'G', consensus.bandwidth_weights) / 10000.0))

            consensus_table['WeightForMiddle'].append(relay.bandwidth * (get_bw_weight(relay.flags, 'M', consensus.bandwidth_weights) / 10000.0))

            if not is_exit:
                consensus_table['WeightForExit'].append(0.0)
            else:
                consensus_table['WeightForExit'].append(relay.bandwidth * (get_bw_weight(relay.flags, 'E', consensus.bandwidth_weights) / 10000.0))

        df = pd.DataFrame(consensus_table)
        df = df.set_index('ID')

        df['ProbForGuard'] = df['WeightForGuard'] / df['WeightForGuard'].sum()
        df['ProbForMiddle'] = df['WeightForMiddle'] / df['WeightForMiddle'].sum()
        df['ProbForExit'] = df['WeightForExit'] / df['WeightForExit'].sum()

        subnet_counts = df['Subnet'].value_counts()
        df = df.join(subnet_counts, rsuffix='Count', on='Subnet')

        return Consensus(df, consensus, consensus.valid_after)

    def __calculate_bandwidth_weights(chosen_relays):
        # source: https://github.com/shadow/tornettools/blob/main/tornettools/generate_tor.py#L835
        g_weight = sum([chosen_relays['g'][fp]['weight'] for fp in chosen_relays['g']])
        e_weight = sum([chosen_relays['e'][fp]['weight'] for fp in chosen_relays['e']])
        ge_weight = sum([chosen_relays['ge'][fp]['weight'] for fp in chosen_relays['ge']])
        m_weight = sum([chosen_relays['m'][fp]['weight'] for fp in chosen_relays['m']])

        min_weight = get_min(chosen_relays)

        g_consweight = g_weight / min_weight
        e_consweight = e_weight / min_weight
        ge_consweight = ge_weight / min_weight
        m_consweight = m_weight / min_weight
        T = g_consweight + e_consweight + ge_consweight + m_consweight

        casename, Wgg, Wgd, Wee, Wed, Wmg, Wme, Wmd =\
                recompute_bwweights(g_consweight, m_consweight, e_consweight, ge_consweight, T)

        weights = {
            'Wgd': Wgd,
            'Wgg': Wgg,
            'Wgm': Wgg,

            'Wmg': Wmg,
            'Wme': Wme,
            'Wmd': Wmd,
            'Wmm': 10000,

            'Wed': Wed,
            'Wee': Wee,
        }

        return weights

    def generate(relay_info_path="./relayinfo_staging_2020-11-01--2020-11-30.json", network_scale=0.25):
        (relays_by_tag, total) = get_relays(Args(relay_info_path, network_scale))

        bw_weights = Consensus.__calculate_bandwidth_weights(relays_by_tag)

        consensus_table = defaultdict(list)

        total = 0
        for tag in relays_by_tag:
            for fingerprint in relays_by_tag[tag]:
                relay = relays_by_tag[tag][fingerprint]

                is_guard = False
                is_middle = False
                is_exit = False

                if tag == 'ge':
                    is_guard = True
                    is_exit = True
                elif tag == 'g':
                    is_guard = True
                elif tag == 'm':
                    is_middle = True
                elif tag == 'e':
                    is_exit = True
                else:
                    assert(False)

                flags = []
                if is_guard:
                    flags.append(Flag.GUARD)
                if is_exit:
                    flags.append(Flag.EXIT)

                bandwidth = float(relay['weight'])

                consensus_table['ID'].append(relay['fingerprint'])
                consensus_table['Nickname'].append(relay['nickname'])
                consensus_table['Version'].append(None)

                consensus_table['Fast'].append(None)
                consensus_table['Authority'].append(None)
                consensus_table['Guard'].append(is_guard)
                consensus_table['Middle'].append(is_middle)
                consensus_table['Exit'].append(is_exit)
                consensus_table['V2Dir'].append(None)
                consensus_table['Stable'].append(None)
                consensus_table['HSDir'].append(None)
                consensus_table['BadExit'].append(None)
                consensus_table['StaleDesc'].append(None)
                consensus_table['Running'].append(None)
                consensus_table['Valid'].append(None)
                consensus_table['Bandwidth'].append(bandwidth)
                consensus_table['Unmeasured'].append(None)
                if is_middle:
                    consensus_table['WeightForGuard'].append(0.0)
                else:
                    consensus_table['WeightForGuard'].append(bandwidth * (get_bw_weight(flags, 'G', bw_weights) / 10000.0))

                consensus_table['WeightForMiddle'].append(bandwidth * (get_bw_weight(flags, 'M', bw_weights) / 10000.0))

                if not is_exit:
                    consensus_table['WeightForExit'].append(0.0)
                else:
                    consensus_table['WeightForExit'].append(bandwidth * (get_bw_weight(flags, 'E', bw_weights) / 10000.0))

        df = pd.DataFrame(consensus_table)
        df = df.set_index('ID')

        df['ProbForGuard'] = df['WeightForGuard'] / df['WeightForGuard'].sum()
        df['ProbForMiddle'] = df['WeightForMiddle'] / df['WeightForMiddle'].sum()
        df['ProbForExit'] = df['WeightForExit'] / df['WeightForExit'].sum()

        return Consensus(df, BWOnly(bandwidth_weights=bw_weights), datetime.now())

    def relays(self):
        return self.df

    def guards(self, by_flag=False):
        if by_flag:
            return self.df[self.df['Guard']]
        else:
            return self.df[self.df['WeightForGuard'] > 0.0]

    def middles(self, by_flag=False):
        if by_flag:
            return self.df[self.df['Middle']]
        else:
            return self.df[self.df['WeightForMiddle'] > 0.0]

    def exits(self, by_flag=False):
        if by_flag:
            return self.df[self.df['Exit']]
        else:
            return self.df[self.df['WeightForExit'] > 0.0]


class Simulation:
    def __init__(self, guards, middles, exits, date, N=ACTIVE_CIRCUITS_PER_SECOND, k=1,
            reuse_guards=False, consensus=None, is_internal=False,
            directed_links=True):
        assert isinstance(N, int)
        assert isinstance(k, int)
        self.consensus = consensus
        self.guards = guards
        self.middles = middles
        self.exits = exits
        self.date = date
        self.N = N
        self.k = k
        self.reuse_guards = reuse_guards
        self.is_internal = is_internal
        self.directed_links = directed_links
        # NOTE: only valid after call to `simulate()`
        self.stat = None
        self.circuits = None

    def from_consensus(consensus, N=ACTIVE_CIRCUITS_PER_SECOND, k=1, reuse_guards=False, is_internal=False, directed_links=True, by_flag=False):
        return Simulation(
            consensus.guards(by_flag=by_flag),
            consensus.middles(by_flag=by_flag),
            consensus.exits(by_flag=by_flag),
            consensus.date,
            N=N,
            k=k,
            reuse_guards=reuse_guards,
            consensus=consensus,
            is_internal=is_internal,
            directed_links=directed_links
        )

    def merge(sims, directed_links=True):
        sim = Simulation([], [], [], [])
        sim.consensus = []
        sim.guards = []
        sim.middles = []
        sim.exits = []
        sim.date = []
        sim.N = []
        sim.k = []
        sim.reuse_guards = []
        sim.is_internal = []
        sim.directed_links = None
        sim.stat = None
        sim.circuits = []

        for s in sims:
            assert s.stat is not None
            assert s.circuits is not None

            sim.consensus.append(s.consensus)
            sim.guards.append(s.guards)
            sim.middles.append(s.middles)
            sim.exits.append(s.exits)
            sim.date.append(s.date)
            sim.N.append(s.N)
            sim.k.append(s.k)
            sim.reuse_guards.append(s.reuse_guards)
            sim.is_internal.append(s.is_internal)
            sim.circuits += s.circuits

        sim.stat = Stat.create(
            sim.circuits,
            date=None,
            consensus=sim.consensus,
            directed_links=directed_links
        )

        return sim

    def sample_relays_by_probability(df, prob_column, N=ACTIVE_CIRCUITS_PER_SECOND):
        samples = choice(df.index, N, p=df[prob_column])
        return samples

    def simulate(self):
        assert (self.N % self.k) == 0
        guards = Simulation.sample_relays_by_probability(self.guards, 'ProbForGuard', self.N // self.k)
        guards = list(guards)*self.k

        middles = Simulation.sample_relays_by_probability(self.middles, 'ProbForMiddle', self.N)

        if self.is_internal:
            exits = Simulation.sample_relays_by_probability(self.middles, 'ProbForMiddle', self.N)
        else:
            exits = Simulation.sample_relays_by_probability(self.exits, 'ProbForExit', self.N)

        self.circuits = list(zip(guards, middles, exits))

        self.stat = Stat.create(self.circuits,
                date=self.date, consensus=self.consensus,
                directed_links=self.directed_links)

        return self
