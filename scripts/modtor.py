import os
import sys
import shutil
import subprocess
import tarfile
import getpass
import argparse

from datetime import datetime, timedelta
from collections import defaultdict
from glob import glob

class Cert:
    @staticmethod
    def parse_date(key, line):
        try:
            index = line.index(key)
            date = line[len(key):].strip()
            return datetime.strptime(date.decode('utf8'), '%Y-%m-%d %H:%M:%S')
        except ValueError:
            return None

    def __init__(self, lines):
        self.lines = lines
        for line in lines:
            published = Cert.parse_date(b'dir-key-published ', line)
            if published:
                self.published = published
            expires = Cert.parse_date(b'dir-key-expires ', line)
            if expires:
                self.expires = expires

def build_certs(dirname, archive_filename, dt):
    print(f'use {archive_filename}')
    with open(os.path.join(dirname, 'cached-certs'), 'wb') as certs, tarfile.open(archive_filename, "r:xz") as tar:
        for info in tar:
            path = info.name
            if info.isfile():
                filename = os.path.basename(path)
                with tar.extractfile(info) as f:
                    cert = Cert(f.readlines())
                    if cert.published < dt < cert.expires:
                        for line in cert.lines:
                            if len(line) > 0 and line[0] != ord('@'):
                                certs.write(line)

def build_microdesc_consensus(dirname, archive_filename, dt):
    print(f'use {archive_filename}')
    with tarfile.open(archive_filename, "r:xz") as tar:
        filename_pattern = 'microdescs-%Y-%m/consensus-microdesc/%d/%Y-%m-%d-%H-00-00-consensus-microdesc'
        filename = dt.strftime(filename_pattern)
        print(f'use consensus {filename}')
        info = tar.getmember(filename)
        with open(os.path.join(dirname, 'cached-microdesc-consensus'), 'wb') as microdesc_consensus, tar.extractfile(info) as f:
            lines = iter(f.readlines())
            next(lines)
            for line in lines:
                microdesc_consensus.write(line)

def build_microdesc(dirname, archive_filename, dt):
    print(f'use {archive_filename}')
    with open(os.path.join(dirname, 'cached-microdescs'), 'wb') as mircodesc, tarfile.open(archive_filename, "r|xz") as tar:
        for info in tar:
            path = info.name
            if info.isfile() and '/micro/' in path:
                with tar.extractfile(info) as f:
                    mircodesc.write(bytes(dt.strftime('@last-listed %Y-%m-%d %H:%M:%S\n'), 'utf8'))
                    for line in f.readlines():
                        if len(line) > 0 and line[0] != ord('@'):
                            mircodesc.write(line)

def build(dirname, dt):
    build_certs(dirname, 'archive/certs.tar.xz', dt)
    build_microdesc_consensus(dirname, dt.strftime('archive/microdescs-%Y-%m.tar.xz'), dt)
    build_microdesc(dirname, dt.strftime('archive/microdescs-%Y-%m.tar.xz'), dt)

def launch(dt, args):
    key = dt.strftime('%Y-%m-%d-%H-%M-%S')
    dirname = os.path.join('data', key)
    shutil.rmtree(dirname, ignore_errors=True)
    os.makedirs(dirname, exist_ok=True)
    print('build...')
    build(dirname, dt)
    print('start...')
    faketime = dt + timedelta(hours=1, minutes=30)
    with open(os.path.join(dirname, 'log'), 'w') as f:
        try:
            subprocess.run(['sudo', 'ip', 'netns', 'del' , key])
            subprocess.run(['sudo', 'ip', 'netns', 'add' , key])
            subprocess.run(['sudo', 'ip', 'netns', 'exec', key, 'ip', 'link', 'set', 'lo', 'up'])

            launch_tor = ['sudo', 'ip', 'netns', 'exec', key,
                'faketime', faketime.strftime('%Y-%m-%d %H:%M:%S'), 'runuser', '-u', getpass.getuser(), '--',
                './vendor/tor/src/app/tor', '--ClientOnly', '1', '--DataDirectory', dirname
            ]

            if args.internal:
                launch_tor.extend(['--TestCreateInternalCircuits', '1'])

            subprocess.run(launch_tor, stdout=f, stderr=f)
        finally:
            subprocess.run(['sudo', 'ip', 'netns', 'del' , key])

try:
    parser = argparse.ArgumentParser()
    parser.add_argument("--internal", action='store_true', help="create internal circuits")
    parser.add_argument("DATE", help="format %Y-%m-%d %H:%M:%S")
    args = parser.parse_args()

    try:
        dt = datetime.strptime(args.DATE, '%Y-%m-%d %H:%M:%S')
    except ValueError as e:
        print('Argument DATE: ' + str(e))
        exit(0)

    print('launch at %s' % dt.strftime('%Y-%m-%d %H:%M:%S'))
    launch(dt, args)
except KeyboardInterrupt:
    pass
