from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.server_descriptor import ServerDescriptor
from stem.descriptor.server_descriptor import RouterStatusEntryV3
from stem.exit_policy import MicroExitPolicy
from glob import glob
from collections import defaultdict


def overlap(a, b):
    return min(a[1], b[1]) - max(a[0], b[0])


def bittorrent_can_exit_to(relay, group):
    only_accept = True
    if relay.exit_policy.is_exiting_allowed:
        for port_interval in group:
            for rule in relay.exit_policy:
                only_accept &= rule.is_accept
                if overlap((rule.min_port, rule.max_port), port_interval) >= 0:
                    return rule.is_accept
    return not only_accept


groups = {
    'btb': [(6881,6889)],
    'bte': [(6890,6999)],
    'ofs': [(1214,1214),(4661,4666),(6346,6429),(6699,6699)],
    'var': [(25,25),(119,119),(135,139),(445,445),(563,563)],
}


for filename in glob("./archive/**/**/**/*-01-00-00-consensus"):
    consensus = next(parse_file(
        filename,
        descriptor_type = 'network-status-consensus-3 1.0',
        document_handler = DocumentHandler.DOCUMENT,
    ))

    relays = 0
    exits = 0
    exits_bandwidth = 0
    can_exit = 0
    can_exit_bandwidth = 0
    can_exit_bittorrent = 0
    can_exit_bittorrent_policy = 0
    for fingerprint, relay in consensus.routers.items():
        relays += 1
        if Flag.EXIT in relay.flags:
            exits +=1
            exits_bandwidth += relay.bandwidth

            if relay.exit_policy.can_exit_to(address=None, port=80) and relay.exit_policy.can_exit_to(address=None, port=443):
                can_exit += 1
                can_exit_bandwidth += relay.bandwidth

            if bittorrent_can_exit_to(relay, groups['bte']):
                can_exit_bittorrent += 1

            chain = True
            for port in range(6890, 7000):
                chain &= relay.exit_policy.can_exit_to(address=None, port=port)
            if chain:
                can_exit_bittorrent_policy += 1

    print('%s relays=%d exits=%d can_exit=%d can_not_exit=%d can_exit_bittorrent=%d/%d bandwidth_ratio=%f' % (
        filename, relays, exits, can_exit, exits - can_exit, can_exit_bittorrent,
        can_exit_bittorrent_policy, can_exit_bandwidth/exits_bandwidth
    ), end=' ')

    addresses = defaultdict(list)
    for fingerprint, relay in consensus.routers.items():
        addresses[relay.address].append(relay)

    same_ip = 0
    port_equal = 0
    fingerprints_equal = 0
    counts = defaultdict(int)
    for address, relays in addresses.items():
        count = len(relays)
        counts[count] += 1
        if count > 1:
            same_ip += count
            or_ports = set()
            fingerprints = set()
            for relay in relays:
                or_ports.add(relay.or_port)
            for relay in relays:
                fingerprints.add(relay.fingerprint)
            if len(or_ports) == 1:
                port_equal += count
            if len(fingerprints) == 1:
                fingerprints_equal += count
    print('same_ip=%d port_equal=%d fingerprints_equal=%d %s' % (same_ip, port_equal, fingerprints_equal, counts))
